#!/bin/bash

post_date=$(date '+%Y-%m-%d')
post_separator='-';
post_title=$1;
post_end='.md';

file=$post_date$post_separator$post_title$post_end;

touch $file

echo '---'>>$file;
echo 'layout:	post'>>$file;
echo 'title:	'>>$file;
echo 'categories:	[blog]'>>$file;
echo '---'>>$file;
