---
layout: page
title: About
permalink: /about/
---

This is my personal webpage.

I'm a CS undergraduate with interests in Digital Rights and CyberSecurity.

I love tweaking with Arduinos and hardware stuff and will probably write about it some time in my [blog](/blog).

I also really like music, and I try to keep some kind of routine of discovering new and amazing artists, I'll
post about it some time.

